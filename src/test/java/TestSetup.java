import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestSetup {
    @Test
    void testShouldAssertNumberWithSameNumber() {
        assertThat(1, is(equalTo(1)));
        Assertions.assertEquals(1, 1);
    }
}
